provider "aws" {
  region     = var.region
}

resource "aws_s3_bucket" "backup-pfsense" {
  bucket = var.bucket_name
  acl    = "private"

    lifecycle_rule {
    enabled = true
    expiration {
      days = 90
    }
  }
}

resource "aws_iam_user" "backup_pfsense_s3" {
  name = "backup_pfsense_s3"
}

resource "aws_iam_policy" "backup-pfsense-s3" {
  name        = "backup-pfsense-s3"
  description = "Pfsense Config Backup"

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
       {
           "Sid": "",
           "Effect": "Allow",
           "Action": [
               "s3:*"
           ],
           "Resource": [
               "*"

           ]
       }
   ]
}
EOF
}

resource "aws_iam_user_policy_attachment" "backup_pfsense_s3" {
  user       = aws_iam_user.backup_pfsense_s3.name
  policy_arn = aws_iam_policy.backup-pfsense-s3.arn
}
