## Pfsense Backup to S3

Upload your pfsense Backups to S3

Prerequisite

* Running pfsense
* ssh-key configured with a pfsense user
* terraform installed
* ansible installed

1. Apply the needed aws resources

Ensure your awscli is configured

```
cd terraform
terraform init
terraform apply
```

2. Prepare Ansible

Fill the secets.yml with your Security Credentials from the AWS User see terraform code
```
ansible-vault encrypt ansible/backup/vars/secrets.yml
```

Fill the hosts file with your pfsense
```
pfsense ansible_user=PFSENSE_USER
```

Set the S3 Bucket Name in site.yml
```
  vars:
    s3_bucket_name: BUCKET_NAME
```

Execute the playbook
```
ansible-playbook -D -i ansible/hosts ansible/site.yml --ask-vault-pass
```
